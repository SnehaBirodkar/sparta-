new WOW().init();
$(function () {

    document.onscroll = scroll;

    function scroll(e) {
        if (scrollY != 0) {
            $(".top-btn").css("display", "block");
        } else {
            $(".top-btn").css("display", "none");
        }
    }
    $(".fa-circle").click(function (e) {
        //                alert("hii");
        $(".fa-circle").css("color", "#EEE");
        $(".js").fadeOut(500);
        $(".js").fadeIn(1000);


        let val = e.target;
        console.log(val);
        $(e.target).css("color", "#b0914f");
    });

    $(".top-btn").click(function () {
        setInterval(function () {
            scroll(0, scrollY - 10);
        }, 10);
    });

    $(".image__btn-right , .image__btn-left").click(function () {
        $(".image").fadeOut(300);
        $(".image1").fadeIn(300);
    });

    $(".image1__btn-right , .image1__btn-left").click(function () {
        $(".image1").fadeOut(300);
        $(".image").fadeIn(300);
    });

    $(".header__links__hamburger").click(function () {
        $(".hamburger-menu").fadeIn(300);
    });

    $(".hamburger-menu__icon").click(function () {
        $(".hamburger-menu").fadeOut(500);
    });

});
